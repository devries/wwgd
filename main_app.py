#!/usr/bin/env python
import bottle
import requests
import json
import os
import oauth_client
import urlparse
import redis
import bottle_session
import random
import string

app = bottle.app()

redis_url = urlparse.urlparse(os.environ.get('REDISCLOUD_URL','http://localhost:6379'))
client_id = os.environ.get('GOOGLE_CLIENT_ID')
client_secret = os.environ.get('GOOGLE_CLIENT_SECRET')
token_url = 'https://accounts.google.com/o/oauth2/token'
id_info_url = 'https://www.googleapis.com/plus/v1/people/me'

redis_connection_pool = redis.ConnectionPool(host=redis_url.hostname,port=redis_url.port,password=redis_url.password)

session_plugin = bottle_session.SessionPlugin(cookie_lifetime=600)
session_plugin.connection_pool = redis_connection_pool

app.install(session_plugin)

@bottle.route('/')
def index(session):
    state = ''.join(random.choice(string.ascii_uppercase+string.digits) for x in range(32))
    session['state']=state

    return bottle.template('index',state=state,client_id=client_id)

@bottle.route('/connect', method='POST')
def connect(session):
    returned_state = bottle.request.query.state

    if returned_state != session['state']:
        return {'info':'Session/Web Page call mismatch.'}

    auth = oauth_client.Oauth2()
    auth.token_url = token_url
    auth.client_id = client_id
    auth.client_secret = client_secret
    auth.redirect_uri = 'postmessage'

    code = bottle.request.body.read()

    auth.obtain_tokens(code)

    session.regenerate()

    r = requests.get('https://www.googleapis.com/plus/v1/people/me',auth=auth)

    user_data = r.json()

    response = {'info':parse_to_html(user_data)}
    return response

def parse_to_html(parsable_structure):
    attributes = []
    if isinstance(parsable_structure,dict):
        attributes.append('<ul>')
        keys = parsable_structure.keys()
        for k in keys:
            attributes.append('<li><b>%s:</b> %s'%(k,parse_to_html(parsable_structure[k])))

        attributes.append('</ul>')
    elif isinstance(parsable_structure,list):
        attributes.append('<ol>')
        for item in parsable_structure:
            attributes.append('<li> %s'%parse_to_html(item))

        attributes.append('</ol>')

    else:
        attribute = str(parsable_structure)
        scheme = urlparse.urlparse(attribute).scheme

        if scheme=='http' or scheme=='https':
            attributes.append('<a href="%s">%s</a>'%(attribute,attribute))
        else:
            attributes.append(attribute)

    return ''.join(attributes)
        
if __name__ == "__main__":
    bottle.debug(True)
    bottle.run(app=app,host='localhost',port=9021)
