#!/usr/bin/env python
import requests
import urllib
import json

class Oauth2(requests.auth.AuthBase):
    def __init__(self):
        self.auth_url = None
        self.client_id = None
        self.client_secret = None
        self.redirect_uri = None
        self.token_url = None
        self.revoke_url = None
        self.access_token = None
        self.refresh_token = None

    def set_credentials(self,client_id,client_secret,redirect_uri):
        self.client_id = client_id
        self.client_secret = client_secret
        self.redirect_uri = redirect_uri

    def set_urls(self,auth_url,token_url,revoke_url=None):
        self.auth_url = auth_url
        self.token_url = token_url
        self.revoke_url = revoke_url

    def get_full_auth_url(self,additional_parameters=None):
        if self.client_id==None:
            raise NameError('Client ID is not defined')
        if self.redirect_uri==None:
            raise NameError('Redirect URI is not defined')

        parameters = {'response_type':'code',
                'client_id':self.client_id,
                'redirect_uri':self.redirect_uri}

        parameters = dict(parameters.items() + additional_parameters.items())

        if self.auth_url==None:
            raise NameError('Authorization Endpoint URL is not defined')

        full_url = "%s?%s"%(self.auth_url,urllib.urlencode(parameters))

        return full_url

    def obtain_tokens(self,code):
        if self.client_id==None:
            raise NameError('Client ID is not defined')
        if self.client_secret==None:
            raise NameError('Client Secret is not defined')
        if self.redirect_uri==None:
            raise NameError('Redirect URI is not defined')

        params = {'code':code,
                'client_id':self.client_id,
                'client_secret':self.client_secret,
                'redirect_uri':self.redirect_uri,
                'grant_type':'authorization_code'}

        if self.token_url==None:
            raise NameError('Token endpoint URL is not defined')

        r = requests.post(self.token_url,data=params)

        token_object = r.json()

        self.access_token = token_object.get('access_token',None)
        self.refresh_token = token_object.get('refresh_token',None)

        return token_object

    def __call__(self,r):
        if self.access_token==None:
            raise NameError('Access token has not been obtained.')

        r.headers['Authorization'] = 'Bearer %s'%self.access_token
        return r

    def refresh_tokens(self):
        if self.refresh_token==None:
            raise NameError('Refresh Token not found')
        if self.client_id==None:
            raise NameError('Client ID is not defined')
        if self.client_secret==None:
            raise NameError('Client Secret is not defined')

        params = {'client_id':self.client_id,
                'client_secret':self.client_secret,
                'refresh_token':self.refresh_token,
                'grant_type':'refresh_token'}

        if self.token_url==None:
            raise NameError('Token endpoint URL is not defined')

        r = requests.post(self.token_url,data=params)

        token_object = r.json()

        self.access_token = token_object.get('access_token',None)

        return token_object

    def revoke_tokens(self):
        if self.refresh_token!=None:
            token = self.refresh_token
        elif self.access_token!=None:
            token = self.access_token
        else:
            raise NameError('No Token available to revoke')

        if self.revoke_url==None:
            raise NameError('No token revocation endpoint URL found')

        r = requests.get(self.revoke_url,params={'token':token})

        if r.status_code!=200:
            return r.json()
        else:
            self.refresh_token=None
            self.access_token=None
            return None

    def dump_state(self):
        return json.dumps(self.__dict__)

    def load_state(self,dump_string):
        dump = json.loads(dump_string)

        for k,v in dump.items():
            self.__dict__[k]=v
